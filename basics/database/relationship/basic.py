from model import db, Puppy, Toy, Owner

## CREATE PUPPY##
rufus = Puppy('rufus')
fido = Puppy('fido')
db.session.add_all([rufus, fido])
db.session.commit()

print(Puppy.query.all())

# Filters
rufus = Puppy.query.filter_by(name='rufus').first() #all()

## CREATE OWNER
jack = Owner('jack', rufus.id)

toy1 = Toy('Chew toy', rufus.id)
toy2 = Toy('Ball', rufus.id)

db.session.add_all([jack, toy1, toy2])
db.session.commit()

rufus = Puppy.query.filter_by(name='rufus').first()
rufus.report_toys()