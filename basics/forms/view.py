from flask import Flask, session, render_template, redirect, url_for, flash
from model import InfoForm

app = Flask(__name__)

app.config['SECRET_KEY'] = 'mykey'

@app.route('/', methods=['GET', 'POST'])
def index():
    
    form = InfoForm()
    
    if form.validate_on_submit():
        
        flash("Your signed up successfully")
        

        session["email"] = form.email.data
        #flash(f"Your signed up successfully {{session[email]}}")
        session["password"] = form.password.data

        return redirect(url_for('index'))

    return render_template('index.html', form=form)

if __name__ == "__main__":
    app.run(debug=True)