from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField

from wtforms.validators import DataRequired

class InfoForm(FlaskForm):

    email = StringField(label="Email:", validators=[DataRequired()])
    password = PasswordField(label="Password:", validators=[DataRequired()])
    submit = SubmitField(label="Submit")