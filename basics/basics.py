from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return "<h1> Welcome, Go to puppy_latin to see your name in latin format </h1>"

@app.route('/puppy_name/<name>')
def puppylatin(name):

    puppyname = ''
    if name[-1] == 'y':
        puppuname = name[:-1] + 'iful'
    else:
        puppuname = name + 'y'

    return "<h1> Your puppy name is {} </h1>".format(puppuname)

if __name__ == "__main__":
    app.run()