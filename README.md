# Flask

## Pip install on ubuntu
sudo apt update
sudo apt install python3-pip

## Install python on ubuntu
sudo apt-get install python3.6

## Install virtual enviornment on ubuntu
apt install python3.8-venv

## Create a virtual environment
python3 -m venv myenv

## Active your virtual environment:
source venv/bin/activate

## To deactivate:
deactivate
