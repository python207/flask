from flask import Blueprint, render_template, redirect, url_for
from Solution import db
from Solution.models import Owners
from Solution.owners.forms import AddForm

owner_blueprint = Blueprint("owners", __name__, template_folder='templates/owner')

@owner_blueprint.route('/add', methods=['GET', 'POST'])
def add():
    form = AddForm()

    if form.validate_on_submit():
        name = form.name.data
        id = form.id.data

        new_owner = Owners(name, id)
        db.session.add(new_owner)
        db.session.commit()

        return redirect(url_for('puppies.list'))

    return render_template('add.html', form = form)