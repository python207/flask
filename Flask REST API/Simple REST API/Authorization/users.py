class User():

    def __init__(self, id, username, password) -> None:
        
        self.id = id
        self.password = password
        self.username = username
    
    def __str__(self) -> str:
        return f"User ID: {self.id}"