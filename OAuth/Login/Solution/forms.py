from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, Email, EqualTo
from wtforms import ValidationError
from Solution.models import User

class LoginForm(FlaskForm):
    email = StringField('Email:', validators=[DataRequired(), Email()])
    password = PasswordField('Password:', validators=[DataRequired()])
    submit = SubmitField('Login')


class RegistrationForm(FlaskForm):

    email = StringField('Email:', validators=[DataRequired(), Email()])
    username = StringField('Username:', validators=[DataRequired()])
    password = PasswordField('Password:', validators=[DataRequired(), EqualTo('password_confirm', message='Password must match!')])
    password_confirm = PasswordField('Confirm Password:', validators=[DataRequired()])
    submit = SubmitField('Register')


    def email_check(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Your email has already registered')

    def email_username(self, field):
        if User.query.filter_by(username=field.data).first():
            raise ValidationError('Your username has already taken')